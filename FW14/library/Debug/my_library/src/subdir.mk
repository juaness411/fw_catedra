################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../my_library/src/button_status.c \
../my_library/src/user_led.c 

OBJS += \
./my_library/src/button_status.o \
./my_library/src/user_led.o 

C_DEPS += \
./my_library/src/button_status.d \
./my_library/src/user_led.d 


# Each subdirectory must supply rules for building sources it contributes
my_library/src/%.o my_library/src/%.su my_library/src/%.cyclo: ../my_library/src/%.c my_library/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/User/Desktop/Catedra/FW/FW_JuanEstebanSarmientoQuintero/FW14/library/my_library/inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-my_library-2f-src

clean-my_library-2f-src:
	-$(RM) ./my_library/src/button_status.cyclo ./my_library/src/button_status.d ./my_library/src/button_status.o ./my_library/src/button_status.su ./my_library/src/user_led.cyclo ./my_library/src/user_led.d ./my_library/src/user_led.o ./my_library/src/user_led.su

.PHONY: clean-my_library-2f-src

