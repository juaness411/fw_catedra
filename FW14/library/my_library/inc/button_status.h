/**
 * @file button_status.h
 * @author Juan Esteban Sarmiento Quintero
 * @brief this file contains the declaration of the functions and private define
 *        to be used in the file button_status.c
 * @version 1.0
 * @date 2023-05-21
 */

#ifndef INC_BUTTON_STATUS_H_
#define INC_BUTTON_STATUS_H_

/* Private define ------------------------------------------------------------*/
#define PUSH_BUTTON_GPIO_Port GPIOA
#define PUSH_BUTTON_Pin GPIO_PIN_0

/* Private function ----------------------------------------------------------*/
void GPIO_BUTTON_Init(void);
double STATUS_BUTTON(button_on_off);


#endif /* INC_BUTTON_STATUS_H_ */
