/**
 * @file user_led.h
 * @author Juan Esteban Sarmiento Quintero
 * @brief this file contains the declaration of the functions and private define
 *        to be used in the file user_led.c
 * @version 1.0
 * @date 2023-05-21
 */

#ifndef INC_USER_LED_H_
#define INC_USER_LED_H_

/* Private define ------------------------------------------------------------*/
#define USER_LED_Pin GPIO_PIN_14
#define USER_LED_GPIO_Port GPIOG

/* Private function ----------------------------------------------------------*/
void GPIO_LED_Init(void);
double LED_ON_OFF(is_led_on);

#endif /* INC_USER_LED_H_ */
