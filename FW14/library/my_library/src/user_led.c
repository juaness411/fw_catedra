/**
 * @file user_led.c
 * @author Juan Esteban Sarmiento Quintero
 * @brief this file contains the definition of the functions
 *        to write the state (on/off) of the led
 * @version 1.0
 * @date 2023-05-21
 */

/* Private includes ----------------------------------------------------------*/
#include "main.h"
#include "user_led.h"
#include <stdbool.h>

/* Private variables ---------------------------------------------------------*/
uint32_t is_led_on;

/**
  * @brief  This function writes to the LED to turn it on or off
  * @param  uint32_t is_led_on
  */

double LED_ON_OFF(is_led_on)
{
	if (is_led_on == 0)
	{
		HAL_GPIO_WritePin(USER_LED_GPIO_Port, USER_LED_Pin, GPIO_PIN_RESET);
	}
	else if (is_led_on == 1)
	{
		HAL_GPIO_WritePin(USER_LED_GPIO_Port, USER_LED_Pin, GPIO_PIN_SET);
	}
}

/**
  * @brief This function initializes and configures
  *        the gpio parameters for the use of the led
  */

void GPIO_LED_Init(void)
{
	  GPIO_InitTypeDef GPIO_InitStruct = {0};
	/* USER CODE BEGIN MX_GPIO_Init_1 */
	/* USER CODE END MX_GPIO_Init_1 */

	  /* GPIO Ports Clock Enable */
	  __HAL_RCC_GPIOA_CLK_ENABLE();
	  __HAL_RCC_GPIOG_CLK_ENABLE();

	  /*Configure GPIO pin Output Level */
	  HAL_GPIO_WritePin(USER_LED_GPIO_Port, USER_LED_Pin, GPIO_PIN_RESET);

	  /*Configure GPIO pin : USER_LED_Pin */
	  GPIO_InitStruct.Pin = USER_LED_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(USER_LED_GPIO_Port, &GPIO_InitStruct);

	/* USER CODE BEGIN MX_GPIO_Init_2 */
	/* USER CODE END MX_GPIO_Init_2 */
}
