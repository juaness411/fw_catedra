/**
 * @file button_status.c
 * @author Juan Esteban Sarmiento Quintero
 * @brief this file contains the definition of the functions to read
 *        the state of the user button
 * @version 1.0
 * @date 2023-05-21
 */

/* Private includes ----------------------------------------------------------*/
#include "main.h"
#include "button_status.h"
#include <stdbool.h>

/* Private variables ---------------------------------------------------------*/
uint32_t button_on_off;

/**
  * @brief  This function reads the state of the button
  * @param  uint32_t button_on_off
  */

double STATUS_BUTTON(button_on_off)
{
	button_on_off = HAL_GPIO_ReadPin(PUSH_BUTTON_GPIO_Port, PUSH_BUTTON_Pin);
	return button_on_off;
}

/**
  * @brief This function initializes and configures
  *        the gpio parameters for the use of the button
  */
void GPIO_BUTTON_Init(void)
{
	  GPIO_InitTypeDef GPIO_InitStruct = {0};
	/* USER CODE BEGIN MX_GPIO_Init_1 */
	/* USER CODE END MX_GPIO_Init_1 */

	  /* GPIO Ports Clock Enable */
	  __HAL_RCC_GPIOA_CLK_ENABLE();
	  __HAL_RCC_GPIOG_CLK_ENABLE();

	  /*Configure GPIO pin : BLUE_BUTTON_Pin */
	  GPIO_InitStruct.Pin = PUSH_BUTTON_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  HAL_GPIO_Init(PUSH_BUTTON_GPIO_Port, &GPIO_InitStruct);
	}

