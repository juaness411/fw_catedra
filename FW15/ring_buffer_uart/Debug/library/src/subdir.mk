################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../library/src/command_parser.c \
../library/src/ring_buffer.c \
../library/src/uart_driver.c 

OBJS += \
./library/src/command_parser.o \
./library/src/ring_buffer.o \
./library/src/uart_driver.o 

C_DEPS += \
./library/src/command_parser.d \
./library/src/ring_buffer.d \
./library/src/uart_driver.d 


# Each subdirectory must supply rules for building sources it contributes
library/src/%.o library/src/%.su library/src/%.cyclo: ../library/src/%.c library/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/User/Desktop/Catedra/FW/FW_JuanEstebanSarmientoQuintero/FW15/ring_buffer_uart/library/inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-library-2f-src

clean-library-2f-src:
	-$(RM) ./library/src/command_parser.cyclo ./library/src/command_parser.d ./library/src/command_parser.o ./library/src/command_parser.su ./library/src/ring_buffer.cyclo ./library/src/ring_buffer.d ./library/src/ring_buffer.o ./library/src/ring_buffer.su ./library/src/uart_driver.cyclo ./library/src/uart_driver.d ./library/src/uart_driver.o ./library/src/uart_driver.su

.PHONY: clean-library-2f-src

