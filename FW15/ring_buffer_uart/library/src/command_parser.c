/*
 * command_parser.c
 *
 *  Created on: May 26, 2023
 *      Author: User
 */

#include "command_parser.h"

#include "uart_driver.h"
#include "main.h"

#include <string.h>

extern uart_driver_t uart_driver;

const uint8_t ack_message[] = "*ACK#";
const uint8_t nack_message[] = "*NACK#";

uint8_t process_set_command(rx_packet_t *rx_packet)
{
	uint8_t ret_val=1;
	switch (rx_packet->element){
		case ELEMENT_LED:
			if(rx_packet->value == '0'){
				HAL_GPIO_WritePin(USER_LED_GPIO_Port, USER_LED_Pin, GPIO_PIN_RESET);
			} else {
				HAL_GPIO_WritePin(USER_LED_GPIO_Port, USER_LED_Pin, GPIO_PIN_SET);
			}
			break;
		default:
			ret_val = 0;
			break;
	}
	return(ret_val);
}

void parse_command(uint8_t *data)
{
	rx_packet_t *packet = (rx_packet_t *)data;
	if(packet->operation == OPERATION_GET){

	}else if(packet->operation == OPERATION_SET){
		if(!process_set_command(packet)){
			uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
		}else{
			uart_driver_send(&uart_driver, (uint8_t *)ack_message, sizeof(ack_message) - 1);
		}
	}else{
		uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
	}
}
