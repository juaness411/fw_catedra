/*
 * command_parser.h
 *
 *  Created on: May 26, 2023
 *      Author: User
 */

#ifndef INC_COMMAND_PARSER_H_
#define INC_COMMAND_PARSER_H_


#include <stdint.h>
#include <stdlib.h>

#include "protocol.h"

void parse_command(uint8_t *);

#endif /* INC_COMMAND_PARSER_H_ */
