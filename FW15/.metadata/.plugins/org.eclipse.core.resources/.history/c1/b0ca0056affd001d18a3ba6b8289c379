/*
 * ring_buffer.h
 *
 *  Created on: May 26, 2023
 *      Author: User
 */

#ifndef INC_RING_BUFFER_H_
#define INC_RING_BUFFER_H_


#include <stdint.h>
#include <stdlib.h>


typedef struct ring_buffer_ {
	uint8_t  *buffer;
	size_t   head;
	size_t   tail;
	size_t   max;
	uint8_t  full;
} ring_buffer_t;


void ring_buffer_reset(ring_buffer_t *);
uint8_t ring_buffer_init(ring_buffer_t *, uint8_t *, size_t);
uint8_t ring_buffer_full(volatile ring_buffer_t *);
uint8_t ring_buffer_put(volatile ring_buffer_t *, uint8_t);
uint8_t ring_buffer_empty(ring_buffer_t *);
uint8_t ring_buffer_get(ring_buffer_t *, uint8_t *);
size_t ring_buffer_size(ring_buffer_t *);
size_t ring_buffer_capacity(volatile ring_buffer_t *);
uint8_t *ring_buffer_fetch(volatile ring_buffer_t *);


#endif /* INC_RING_BUFFER_H_ */
