# FW_CATEDRA

Branch:

+  feature/interrupts

       - FW5 module task, project on uart in interrupt mode.

+  feature/intro

      - FW1 : Document that, describe deeper the difference between CISC and RISC, describe deeper the difference between 8, 16, and 32 bits architectures, explain the distribution of the                     memories of a specific MCU family implementation (Ex: STM32). You can choose a FW application to explain.
 
      - FW2 : Exercise on creating a git repository both remotely and online.

      - FW3 : Creating a basic animation for LEDs using the push button on stm32cubeide.

+  feature/spi

     - FW7 module task, temperature and pressure sensor reading through spi.

+  feature/uart_gpio

     - FW4 and FW6 module task, about using gpio and uart.

+  feature/usb

    - FW8 module task, using virtual usb com port.

+  feature/i2c_lcd_sdram

     - FW9 module task, use of lcd screen, i2c communication and sdrm memory.

+ Branch main and develop contains all the projects named above.